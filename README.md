# Bisulfite-independent analysis of CpG island methylation enables genome-scale stratification of single cells #
--------

Raw sequence reads were mapped against the hg19 version of the human reference genome using Bowtie2 with default parameters. Read counts for each InfoFrag, instead of each InfoSite were calculated. Merging reads in each InfoFrag generated with RE1 leads to loss of resolution, but provides greater read counts. Based on the experimental design, the read counts from each InfoFrag as a whole can represent the associated genomic block.  

Therefore, DNA methylation states of InfoFrags in a single cell can be classified into three groups: methylated (M or Me), un-methylated (U or Um), and half-methylated or methylation heterozygote (H or He, one allele is Me and the other allele is Um). For the methylated fragments, similar numbers of reads are sequenced from both TEST and MC samples, which means R_test^i (read counts of fragment i in the TEST sample) is increased with and dependent on R_mc^i (read counts of fragment i in the MC sample). Un-methylated fragments are supposed to have no sequence reads from the TEST sample, but some reads from the MC sample, such that R_test^i is always near 0 and has no correlation with and is not dependent on R_mc^i. Half-methylated fragments account for allele-specific methylation, in which R_test^i increases with and is dependent on R_mc^i, but the reads from the TEST sample are much lower than those from MC sample. Therefore, we applied a three-component mixture model to infer the probability of a fragment belonging to one methylation state using the EM algorithm (code in scCGI-seq.R).


### How to download the code? ###
--------
Download the code using git clone or using the "download" link.

     git clone https://bitbucket.org/mthjwu/sccgi-seq


### How to run scCGI-seq methylation caller? ###
--------
The code is build in R. In order to call methylation score from scCGI-seq data, prepare two matrix in R: tt is the count matrix of InfoFrags for test samples, cc is the count matrix of InfoFrags for control samples.

        	## To get the estimated probability of InfoFrags belonging to the three methylation states.
			mprob<-scMETcall3group(tt, cc)

			## get the M score of InfoFrags
			mcall <- sapply(mprob$res, function(x){
				x[,3]+x[,2]/2
			})


### Who do I talk to? ###
--------
* Hua-Jun Wu (hjwu@jimmy.harvard.edu)

